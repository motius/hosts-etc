Bonjour à tous !
Le contenu de ce dépôt est très simple ! Il s'agit d'un fichier hosts. 
Ce fichier existe sous Linux/Mac/Windows (et probablement les autres).
Il permet de faire de la résolution statique de noms de domaine.
Il est donc le point de départ de toutes requête DNS qui part de votre ordinateur.
Ce pourquoi j'y ai rajouté (et y rajouterai encore) les gros serveurs de pubs sur le web.
De cette manière, vos application effectuant une requête vers un serveur de pub verront 127.0.0.1 à la place.
127.0.0.1 est localhost. J'y ai (bien sûr) aussi mis ::1 (le même localhost en IPv6).
Bonne navigation sur les interwebs !
Motius